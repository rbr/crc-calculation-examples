# CRC Calculation Examples

## Introduction

One of the most tedious things
about implementing data downloads
from RBR instruments
on a new platform
is usually getting CRC calculations to work.
You've got to go rooting through other projects
to find an isolated implementation
of the calculation function,
and maybe you don't have one on hand
for the target language,
so you have to port one,
and then the port isn't quite right,
and you're working from real instrument data,
not reference data,
so you don't know what the right CRC value is,
and you forgot that the CRC is big endian
so you need to swap the bytes coming from the instrument...

This is a small library
(in the organizational sense,
not the software development sense)
of CRC test cases
(binary blobs)
and reference implementations
in a variety of languages:

* C
* Java
* Python

## Building

See each implementation
for specific build instructions.

## Contributing

The library is primarily maintained by RBR
and development is directed by our needs
and the needs of our [OEM] customers.
However, we're happy to take [contributions] generally.

[OEM]: https://rbr-global.com/products/oem
[contributions]: CONTRIBUTING.md

## License

This project is licensed under the terms
of the Apache License, Version 2.0;
see https://www.apache.org/licenses/LICENSE-2.0.
