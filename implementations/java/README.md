# CRC Calculation Example: Java

## Introduction

This is a simple example
of CRC calculation in Java.

Both CRC calculation functions
and the executable entry point
live in `src/main/java/com/rbrglobal/oem/crc/Crc.java`.

## Building

The project can be built
with [Maven]:

    $ mvn package

[Maven]: https://maven.apache.org/

## Using

The generated Java library
is executable:

    $ java -jar target/crc-1.0.0.jar <../../tests/256-1.bin
    0x0000
    $ echo $?
    0

The program behaviour
is the same as the C example.
