package com.rbrglobal.oem.crc;

import java.io.IOException;

/**
 * A CRC calculated for a block of data.
 */
public class Crc
{
    private static final int CRC_INITIAL_VALUE = 0xFFFF;
    private static final int CRC_POLYNOMIAL = 0x1021;

    private static final int BLOCK_SIZE = 8192;

    private int crc = Crc.CRC_INITIAL_VALUE;

    /**
     * Initialize a CRC with no data. The value reported by {@link #getCrc()}
     * will be a default.
     */
    public Crc()
    {
    }

    /**
     * Calculate the CRC for a block of data.
     *
     * @param data the block of data
     */
    public Crc(byte[] data)
    {
        this.update(data, 0, data.length);
    }

    /**
     * Calculate the CRC for a block of data.
     *
     * @param data the block of data
     * @param offset the offset to read from in <code>data<code>
     * @param length the length to read from <code>data</code>
     * @throws IndexOutOfBoundsException if <code>offset</code> is negative,
     *                                   <code>length</code> is negative, or
     *                                   <code>length</code> is greater than
     *                                   <code>data.length - offset</code>
     */
    public Crc(byte[] data, int offset, int length)
    {
        this.update(data, offset, length);
    }

    /**
     * Get the calculated CRC value.
     *
     * @return the calculated CRC value
     */
    public int getCrc()
    {
        return this.crc;
    }

    /**
     * Feed an existing CRC result with more data.
     *
     * @param data the block of data
     */
    public void update(byte[] data)
    {
        this.update(data, 0, data.length);
    }

    /**
     * Feed an existing CRC result with more data.
     *
     * @param data the block of data
     * @param offset the offset to read from in <code>data<code>
     * @param length the length to read from <code>data</code>
     * @throws IndexOutOfBoundsException if <code>offset</code> is negative,
     *                                   <code>length</code> is negative, or
     *                                   <code>length</code> is greater than
     *                                   <code>data.length - offset</code>
     */
    public void update(byte[] data, int offset, int length)
    {
        if (offset < 0
            || length < 0
            || length > data.length - offset)
        {
            throw new IndexOutOfBoundsException();
        }

        for (int i = offset; i < offset + length; i++)
        {
            byte b = data[i];

            for (int j = 0; j < 8; j++)
            {
                boolean bit = ((b   >> (7 - j) & 1) == 1);
                boolean c15 = ((crc >> 15      & 1) == 1);

                crc = (crc << 1) & 0xFFFF;

                if (c15 ^ bit)
                {
                    crc ^= Crc.CRC_POLYNOMIAL;
                }
            }
        }
    }

    /**
     * CRC calculation example.
     *
     * Calculates the CRC for the entirety of stdin and prints the calculated
     * value to stdout. If the CRC is 0 (check pass, if an expected CRC value
     * is included as the last two bytes), then the program returns 0;
     * otherwise, returns 1.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) throws IOException
    {
        byte[] buf = new byte[BLOCK_SIZE];

        Crc crc = new Crc();
        int read;
        while ((read = System.in.read(buf)) > 0)
        {
            crc.update(buf, 0, read);
        }

        System.out.printf("0x%04X\n", crc.getCrc());
        System.exit(crc.getCrc() == 0 ? 0 : 1);
    }
}
