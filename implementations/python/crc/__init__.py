#! /usr/bin/env python3

"""
Python example of CRC checking for RBR instrument data.
"""

__version__ = '1.0.0'

__author__ = 'Samuel Coleman'
__contact__ = 'samuel.coleman@rbr-global.com'

__copyright__ = 'Copyright (c) 2018 RBR Ltd.'
__license__ = 'APLv2'

CRC_INITIAL_VALUE = 0xFFFF
CRC_POLYNOMIAL = 0x1021

def init(data):
    """
    Calculate the CRC of a block of data.
    """
    return update(CRC_INITIAL_VALUE, data)

def update(crc, data):
    """
    Feed an existing CRC result with more data.
    """
    for i in range(0, len(data)):
        b = data[i]

        for j in range(0, 8):
            bit = ((b   >> (7 - j) & 1) == 1)
            c15 = ((crc >> 15      & 1) == 1)

            crc = (crc << 1) & 0xFFFF

            if c15 ^ bit:
                crc ^= CRC_POLYNOMIAL

    return crc
