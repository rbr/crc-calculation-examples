#! /usr/bin/env python3

"""
CRC calculation example.

Calculates the CRC for the entirety of stdin and prints the calculated value to
stdout. If the CRC is 0 (check pass, if an expected CRC value is included as
the last two bytes), then the program returns 0; otherwise, returns 1.
"""

import sys

# If we're running from within a wheel (`python crc`) instead of as a module
# (`python -m crc`), we need to make sure the parent directory of the wheel is
# on the path so we can import the wheel.
if __package__ == '':
    import os.path
    path = os.path.dirname(os.path.dirname(__file__))
    sys.path.insert(0, path)

import crc

def main():
    buf = sys.stdin.buffer.read()
    calculated_crc = crc.init(buf)

    print('0x{:04X}'.format(calculated_crc))

    sys.exit(min(1, calculated_crc))

if __name__ == '__main__':
    main()
