# CRC Calculation Example: Python

## Introduction

This is a simple example
of CRC calculation in Python 3.
The core calculation is probably
compatible with Python 2,
but some of the `main` function isn't.

CRC calculation functions
live in `crc/__init__.py`;
the executable entry point
lives in `crc/__main__.py`.

## Using

The package can be run as you'd expect:

    $ python3 crc <../../tests/256-1.bin
    0x0000
    $ echo $?
    0

The program behaviour
is the same as the C example.
