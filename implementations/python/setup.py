#! /usr/bin/env python3

import crc
from setuptools import setup

setup(
    name='rbrcrc',
    version=crc.__version__,

    description=crc.__doc__.strip().split('\n', 1)[0],
    long_description=crc.__doc__.strip(),

    url='https://bitbucket.org/rbr/crc-calculation-examples',

    author=crc.__author__,
    author_email=crc.__contact__,

    license=crc.__license__,

    packages=['crc']
)
