# CRC Calculation Example: C

## Introduction

This is a simple example
of CRC calculation in the C language.
C99 is used only to leverage `stdint.h`;
in all other respects,
the example is ANSI C-compliant.

CRC calculation functions
are declared in `crc.h`
and defined in `crc.c`;
`main.c` provides an example
of how to perform CRC calculation
of a stream.

## Building

The example can be built with GNU Make.
`make` or `make crc` will build the example;
`make clean` will remove the generated binary.

## Using

The program calculates the CRC
of data fed to stdin,
and reports the calculated value
to stdout
as a hexadecimal number.

For error-free input which includes a trailing CRC,
the calculated value will be 0,
so the program will return 0:

    $ ./crc <../../tests/256-1.bin
    0x0000
    $ echo $?
    0

For input which does not include a trailing CRC,
or where the input contains errors,
the calculated value
is a bit more interesting,
and the return value indicates failure:

    $ <../../tests/4096-1.bin head -c -2 | ./crc
    0xF478
    $ echo $?
    1
