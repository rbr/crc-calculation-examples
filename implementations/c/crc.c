/**
 * \file crc.c
 *
 * \brief CRC calculation function definitions.
 *
 * Copyright (c) 2018 RBR Ltd.
 * Licensed under the Apache License, Version 2.0.
 */

#include "crc.h"

#define CRC_INITIAL_VALUE 0xFFFF
#define CRC_POLYNOMIAL 0x1021

CRC CRC_init(const void *data, int32_t size)
{
    return CRC_update(CRC_INITIAL_VALUE, data, size);
}

CRC CRC_update(CRC crc, const void *data, int32_t size)
{
    for (int32_t i = 0; i < size; i++)
    {
        uint8_t b = ((uint8_t *) data)[i];

        for (int_fast8_t j = 0; j < 8; j++)
        {
            uint8_t bit = ((b   >> (7 - j) & 1) == 1);
            uint8_t c15 = ((crc >> 15      & 1) == 1);

            crc <<= 1;

            if (c15 ^ bit)
            {
                crc ^= CRC_POLYNOMIAL;
            }
        }
    }

    return crc;
}
