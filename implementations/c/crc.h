/**
 * \file crc.h
 *
 * \brief CRC calculation function declarations.
 *
 * Copyright (c) 2018 RBR Ltd.
 * Licensed under the Apache License, Version 2.0.
 */

#ifndef CRC_H
#define CRC_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef uint16_t CRC;

/**
 * Calculate the CRC of a block of data.
 *
 * \param [in] data the block of data
 * \param [in] size the size of the block
 * \return the CRC of the block
 */
CRC CRC_init(const void *data, int32_t size);

/**
 * Feed an existing CRC result with more data.
 *
 * \param [in] data the block of data
 * \param [in] crc the previous CRC value
 * \param [in] size the size of the block
 * \return the CRC of the block
 */
CRC CRC_update(CRC crc, const void *data, int32_t size);

#ifdef __cplusplus
}
#endif

#endif /* CRC_H */
