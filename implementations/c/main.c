/**
 * \file main.c
 *
 * \brief CRC calculation example.
 *
 * Calculates the CRC for the entirety of stdin and prints the calculated value
 * to stdout. If the CRC is 0 (check pass, if an expected CRC value is included
 * as the last two bytes), then the program returns 0; otherwise, returns 1.
 *
 * Copyright (c) 2018 RBR Ltd.
 * Licensed under the Apache License, Version 2.0.
 */

/* Required for printf. */
#include <stdio.h>
/* Required for read, STDIN_FILENO. */
#include <unistd.h>

#include "crc.h"

#define BUF_SIZE 8192

uint8_t buf[BUF_SIZE];
ssize_t buf_len = 0;

int main()
{
    CRC crc = CRC_init(buf, buf_len);
    while ((buf_len = read(STDIN_FILENO, buf, BUF_SIZE)) > 0)
    {
        crc = CRC_update(crc, buf, buf_len);
    }

    printf("0x%04X\n", crc);

    return !(crc == 0);
}
